module.exports = {
	root: true,
	env: {
		node: true
	},
	extends: ['plugin:vue/essential', '@vue/standard'],
	globals: {
		$: true,
		$mixin: true,
		$mock: true,
		$production: true,
		$event_bus: true,
		utils: true,
		QRCode: true
	},
	rules: {
		'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-tabs': 'off',
		indent: ['error', 'tab'],
		'no-unused-vars': [
			'error',
			{ vars: 'all', args: 'none', ignoreRestSiblings: false }
		],
		'space-before-function-paren': ['error', 'never'],
		'no-param-reassign': 'off',
		camelcase: 'off'
	},
	parserOptions: {
		parser: 'babel-eslint'
	}
}
