import mixin from '@/mixins/mixin'

window.$mixin = mixin

const utils = {
	deepClone(obj) {
		let isClass = o => {
			if (o === null) return 'Null'
			if (o === undefined) return 'Undefined'
			return Object.prototype.toString.call(o).slice(8, -1)
		}
		let [result, oClass] = [null, isClass(obj)]
		// 确定result的类型
		if (oClass === 'Object') {
			result = {}
		} else if (oClass === 'Array') {
			result = []
		} else {
			return obj
		}
		for (let key in obj) {
			var copy = obj[key]
			if (isClass(copy) === 'Object') {
				result[key] = utils.deepClone(copy) // 递归调用
			} else if (isClass(copy) === 'Array') {
				result[key] = utils.deepClone(copy)
			} else {
				result[key] = obj[key]
			}
		}
		return result
	},

	downLoadFile(relativeUrl){
		window.open('/api/chainFileStore' + relativeUrl)
	}
}
window.utils = utils
window.utils = utils
