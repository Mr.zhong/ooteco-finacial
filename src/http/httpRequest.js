import axios from 'axios'
import 'url-search-params-polyfill'
const proectConfig = require('../../project.config')

let [error, complete] = [null, null]

axios.defaults.baseURL = proectConfig.url
axios.defaults.headers = {
	'X-Requested-With': 'XMLHttpRequest',
	'Content-Type': 'application/x-www-form-urlencoded',
	token: sessionStorage.getItem('userInfo')
		? JSON.parse(sessionStorage.getItem('userInfo')).token
		: ''
}

axios.defaults.timeout = 30000
let [loading, timeStamp] = [null, 0]
axios.interceptors.request.use(
	config => {
		if (['message/updateStatus'].indexOf(config.url) < 0) {
			timeStamp = Date.parse(new Date())
			loading = $event_bus.$loading({
				lock: true,
				background: 'rgba(0, 0, 0, 0.7)'
			})
		}
		return config
	},
	error => {
		return Promise.reject(error)
	}
)

axios.interceptors.response.use(
	response => {
		if (timeStamp - Date.parse(new Date()) < 700) {
			setTimeout(() => {
				loading.close()
			}, 1000)
		} else {
			loading.close()
		}
		complete && complete()
		return response.data
	},
	err => {
		error && error(err)
		complete && complete()
		return Promise.resolve(err.response)
	}
)

export default {
	get(options) {
		axios.defaults.headers['token'] = sessionStorage.getItem('userInfo')
			? JSON.parse(sessionStorage.getItem('userInfo')).token
			: ''
		let [url, params] = [options.url, options.params]

		error = options.error
		complete = options.complete
		if (url.indexOf('detail') > -1) {
			console.log(url)
			console.log(params)
		}
		return new Promise((resolve, reject) => {
			axios({
				method: 'get',
				url,
				params
			}).then(res => {
				if (parseInt(res && res.code) === 0) {
					resolve(res)
				} else {
					reject(res ? res.msg || '系统错误' : '系统错误')
				}
			})
		})
	},
	post(options) {
		axios.defaults.headers['token'] = sessionStorage.getItem('userInfo')
			? JSON.parse(sessionStorage.getItem('userInfo')).token
			: ''
		let [url, data] = [options.url, options.params]
		error = options.error
		complete = options.complete
		let params = new URLSearchParams()
		if ('append' in data) {
			params = data
		} else {
			for (const key in data) {
				if (data.hasOwnProperty(key)) {
					params.append(key, data[key])
				}
			}
		}
		return new Promise((resolve, reject) => {
			axios({
				method: 'post',
				url,
				data: params
				// cancelToken: new CancelToken(c => {
				// 	cancel = c
				// })
			}).then(res => {
				if (parseInt(res && res.code) === 0) {
					resolve(res)
				} else {
					console.log(JSON.stringify(res))
					reject(res ? res.msg || '系统错误' : '系统错误')
				}
			})
		})
	}
}
