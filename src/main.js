import 'babel-polyfill'
import Vue from 'vue'
import '@js/global'
import App from './App.vue'
import router from './routes/router'
import store from './stores/store'

import '@css/init.css'
import 'swiper/dist/css/swiper.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@css/global.css'

import http from '@/http/httpRequest'
import $ from 'jquery'

window.$event_bus = new Vue()
window.$ = $

Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$http = http

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
