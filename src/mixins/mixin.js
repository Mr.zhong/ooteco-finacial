import { mapState } from 'vuex'

let mockData = () => {}
if (process.env.NODE_ENV !== 'production') {
	mockData = require('@/plugins/mock.config')
	mockData = mockData.action
}

export default {
	data() {
		return {}
	},
	created() {
		if (!this.userInfo) {
			if (
				sessionStorage.getItem('userInfo') ||
				this.$route.name === 'index' ||
				this.$route.name === 'face' ||
				this.$route.name === 'register'
			) {
				this.$store.state.userInfo =
					sessionStorage.getItem('userInfo') &&
					JSON.parse(sessionStorage.getItem('userInfo'))
				return false
			}
			this.go('/login')
		}
		// mockData()
	},
	computed: mapState([
		'login',
		'WINDOW_H',
		'WINDOW_W',
		'menu_list',
		'sub_title',
		'userInfo'
	]),
	methods: {
		go(path, query) {
			query = query || {}
			this.$router.push({
				path,
				query
			})
		},
		$log(msg) {
			console.log(msg)
		},
		listen(event_name, callBack) {
			$event_bus.$off(event_name)
			$event_bus.$on(event_name, query => {
				callBack && callBack(query)
			})
		},
		alert(msg, callback) {
			if (callback) {
				this.$alert(msg, '', {
					confirmButtonText: '确定',
					callback: action => {
						callback()
					}
				})
				return false
			}
			if (typeof msg === 'string' && msg.indexOf('成功') > -1) {
				msg = {
					message: msg,
					type: 'success'
				}
			}
			typeof msg === 'object' ? this.$message(msg) : this.$message.error(msg)
		},
		qrcode(url, id = 'qrcode') {
			let qrcode = new QRCode(document.getElementById(id), {
				text: url,
				width: 150,
				height: 150,
				colorDark: '#000000',
				colorLight: '#ffffff',
				correctLevel: QRCode.CorrectLevel.H
			})
			return qrcode
		},
		getFaceVerifyToken(option) {
			this.$http
				.get({
					url: 'user/cacheFaceCode'
				})
				.then(code => {
					let returnUrl =
						'http://' + window.location.host +
						'/#/face?tokenMd5=' +
						this.userInfo.tokenMd5 +
						'&randomCode=' +
						code.data.faceCode
					if (returnUrl.indexOf('http://') < 0) {
						returnUrl = 'http://' + returnUrl
					}
					let userInfo = JSON.parse(sessionStorage.getItem('userInfo'))
					if (option.type === 2) {
						returnUrl +=
							'&companyCode=' +
							option.companyCode +
							'&contractCode=' +
							option.contractCode
						userInfo.operatorUserIdCardNumber = ''
						userInfo.operatorUserName = ''
					}
					this.$http
						.get({
							url: 'tripartite/getBizToken',
							params: {
								id: userInfo.operatorUserIdCardNumber,
								name: userInfo.operatorUserName,
								// id: option.cardNo,
								// name: option.name,
								returnUrl
							}
						})
						.then(res => {
							let urlPath =
								'https://openapi.faceid.com/lite/v1/do/' +
								JSON.parse(res.data).biz_token
							option.callBack && option.callBack(urlPath)
						},
						msg => {
							this.alert(msg)
						})
				})
		}
	}
}
