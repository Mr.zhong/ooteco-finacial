import { createData } from '@/plugins/mock'

import index from './mock.index'

window.$mock = createData

const mock_conf = {
	index
}

const action = () => {
	for (const item of mock_conf['index']) {
		createData(item)
	}
}

export { action }
