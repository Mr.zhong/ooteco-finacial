export default [
	{
		url: '/user/login',
		response: options => {
			//    options 中可获取接口参数  根据接口参数结合mock语法生成数据返回即可
			return {
				code: '0',
				msg: '成功',
				data: {
					companyId: 2,
					companyName: '嘻嘻嘻哈哈有限责任公司y',
					token:
						'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjJfMzU1XzIiLCJleHAiOjE1NDMwNDUxNzcsImlhdCI6MTU0MDQ1MzE3N30.tB7Z9ED40uA9lhlwmJ7TU4_4gVwcsExsKsS3msp5ZrM',
					tokenMd5: '7986a05646bd9847b7ab7411106fefc6',
					lastLoginDate: '2018-10-25 15:39:18',
					riskInfoCompleteRate: 0 //   风险信息完整度百分比
				}
			}
		}
	},
	{
		//  企业入驻
		url: 'company/addCompany',
		response: options => {
			//   companyName  公司名称
			//  companyCreditCode 统一社会信用码
			//  companyAddress 地址
			//  companyCreateTime 成立日期
			//  companyEndTime 到期时间
			//  LegalName 法人
			//  companyType 主体类型
			//  companyCheckResult 核查结果
			//  userType 用户类型 法人还是经办人
			//  legalCardType 证件类型
			//  legalCardNumber 证件号码
			//  legalStartTime 证件开始时间
			//  legalEndTime 证件到期时间
			//  legalGender 性别
			//  leagalNation 民族
			//  legalUserName 法人姓名
			//  legalCardCheckResult 法人身份证核验结果
			//  legalBirthTime 法人出生日期
			//  operatorCardType 经办人证件类型
			//  operatorName 经办人姓名
			//  operatorCardNumber 证件号码
			//  operatorCardStartTime 证件开始日期
			//  operatorCardEndTime 证件到期日
			//  operatorGender 性别
			//  operatorNation 民族
			//  operatorCarcCheckResult 核验结果
			//  operatorRecogniseResult 经办人人脸识别结果
			//  operatorBirthTime 经办人出生日期
			//  BankName 开户行
			//  BankAccountName 账户名
			//  bankAccountNumber 银行账号
			//  province 开户省
			//  city 开户市
			//  openOrganiseName 开户网点
			//  cellphone 电话
			//  verifyCode 验证码
			//  legalUser 法人身份证附件
			//  operator 经办人身份证附件
			//  authorizationFile 法人授权书附件
			//  params 附件信息描述
			return {
				code: '0',
				msg: '成功',
				data: {}
			}
		}
	},
	{
		//  业务列表
		url: '/biz/select/list',
		response: options => {
			/*
        {
            'applyBeginDate': '2018-10-10',  //  非必须 申请开始日期
            'applyEndDate': '2019-01-01',    //  非必须 申请结束日期
            'pageNum': '1',    //  非必须
            'pageSize': '10'  //  非必须
        }
    */
			var pages = 12
			if (!pageNum) {
				pageNum = 1
			}
			if (!pageSize) {
				pageSize = 10
			}
			if (pageNum == pages) {
				return {
					code: '0',
					msg: '成功',
					data: {
						total: pages * pageSize,
						'list|1-pageSize': [
							{
								'id|+1': 136,
								applicant: 5,
								applyTime: '2018-10-25 18:02:27',
								flowPointer: 1,
								approvalStatus: 2,
								applicantName: '郭子俊',
								businessTypeName: '通用',
								businessType: 1,
								approvalStatusName: '已完成',
								buyer: 144,
								seller: 145,
								buyerName: '深圳市潜动力科技有限公司',
								sellerName: '深圳拓垦区块链科技有限公司',
								applyCode: 'BL201810250136',
								makeLoanStatusName: '待放款',
								repayStatusName: '未还款',
								acceptAmount: 100000.0,
								selfApproval: false
							}
						],
						pageNum: pageNum,
						pageSize: pageSize,
						pages: pages,
						isFirstPage: false,
						isLastPage: true,
						hasPreviousPage: true,
						hasNextPage: false
					}
				}
			} else {
				return {
					code: '0',
					msg: '成功',
					data: {
						total: pages * pageSize,
						'list|pageSize': [
							{
								'id|+1': 136,
								applicant: 5,
								applyTime: '2018-10-25 18:02:27',
								flowPointer: 1,
								approvalStatus: 2,
								applicantName: '郭子俊',
								businessTypeName: '通用',
								businessType: 1,
								approvalStatusName: '已完成',
								buyer: 144,
								seller: 145,
								buyerName: '深圳市潜动力科技有限公司',
								sellerName: '深圳拓垦区块链科技有限公司',
								applyCode: 'BL201810250136',
								makeLoanStatusName: '待放款',
								repayStatusName: '未还款',
								acceptAmount: 100000.0,
								selfApproval: false
							}
						],
						pageNum: pageNum,
						pageSize: pageSize,
						pages: pages,
						isFirstPage: pageNum == 1,
						isLastPage: true,
						hasPreviousPage: false,
						hasNextPage: true
					}
				}
			}
		}
	},
	{
		//  业务申请
		url: '/biz/apply',
		response: options => {
			//   options 中可获取接口参数  根据接口参数结合mock语法生成数据返回即可
			/*
    {
        financingAmount: 33223,
        endDate: '2018-10-25', //  业务到期日期
        buyer: 144,
        seller: 144,
        receiptAccount: 3223, //  收款账号
        accountName: 2332, //  账号名称
        accountBranch: 23, //  开户支行
        comment: 2332,
        businessContracts: (binary), //  合同
        endTime: 2018-10-24, //  应收账款到期日期
        amount: 2323, //  应收账款金额
        invoiceJson: [{"buyer":"中金甲子(北京)投资基金管理有限公司","code":"4403173130","number":"16067638","amount":"100000","invoiceDate":"2018年6月13日","checkCode":"","sellerOpenBank":"上海浦东发震银行深圳福田支行79290154740001858","sellerTaxpayerName":"91440300MA5DDP4U88","sellerContactInformation":"深圳市南山区阳光科创中心B座35楼3501室0755-86620031","buyerTaxpayerName":"91110108098166163W","buyerOpenBank":"农业银行北京西长安街支行11211101040000433","buyerContactInformation":"北京市海淀区中关村南大街5号1区689号楼803室85875300","totalPriceAmount":"94339.62","totalTaxAmount":"5660.38","checkPass":true,"taxAttach":"发票.jpeg"}],
        invoices: (binary) //  发票
    }
    */
			return {
				code: '0',
				msg: '成功',
				data: {}
			}
		}
	},
	{
		//  业务详情
		url: '/biz/select/detail',
		response: options => {
			return {
				code: '0',
				msg: '成功',
				data: {
					typeName: '通用', //  业务类型
					factorTypeName: '有追索权明保理', //  保理类型
					financingAmount: 500000.0, //  融资金额
					financingRate: 90.0, //  融资比率
					payInterestTypeName: '前置付息', //  付息方式
					paybackTypeName: '直接回款', //  回款方式
					endDate: '2018-10-26 00:00:00', //  到期日期
					serviceRate: 10.0, //  服务费率
					overDueRate: 24.0, //  逾期利率
					buyerName: '深圳市潜动力科技有限公司', //  买方名称
					sellerName: '深圳拓垦区块链科技有限公司', //  卖方名称
					receiptAccount: 'rrr', //  收款账号
					accountName: 'ee', //  账号名称
					accountBranch: 'www', //  开户行
					comment: '1111',
					createTime: '2018-10-25 18:02:27',
					applicant: '郭子俊',
					haveApprovalButton: false,
					completeApproval: true,
					approvalStatus: 2, //  业务审核状态 1-待审 2-通过 3-拒绝
					approvalFlowPointer: 1, //  业务当前所处审批节点
					financingInterestRate: 10.0,
					attachmentList: [
						{
							id: 2368,
							attachType: 'contracts',
							attachPath: '/home/chainFileStore/20181025/common/136/contracts',
							attachName: '线上合同.pdf',
							companyCode: 'COM1536802996953',
							uploadTime: '2018-10-25 18:02:28',
							uploadUser: 5,
							businessType: 4,
							businessId: 136,
							attachmentKey: 'contracts',
							uploaderCompanyCode: 'COM00000001',
							userName: '郭子俊',
							url: '/20181025/common/136/contracts/线上合同.pdf'
						}
					], //  合同
					applyCode: 'BL201810250136' //  申请单号
				}
			}
		}
	},
	{
		//  应收账款列表 get
		url: '/biz/ac/select/list',
		response: options => {
			return {
				code: '0',
				msg: '成功',
				data: [
					{
						receivableId: 132, //  应收账款id
						endTime: '2018-10-25 18:03:01', //  到期日期
						financingAmount: 90000.0, //  融资金额
						acceptAmount: 100000.0, //  受理金额
						checkStatus: 2,
						businessId: 136,
						loanAmount: 0.0,
						amount: 100000.0,
						checkStatusName: '已审核',
						invoiceDetail: {
							invoices: [
								{
									id: 432,
									subjectId: 132,
									buyer: '中金甲子(北京)投资基金管理有限公司',
									code: '4403173130',
									number: '16067639',
									isCancel: false,
									amount: 100000.0, //  价税合计
									totalTaxAmount: 5660.38,
									totalPriceAmount: 94339.62,
									totalQuantity: 0,
									invoiceDate: '2018-06-13 00:00:00',
									checkCode: '',
									sellerTaxpayerName: '91440300MA5DDP4U88',
									sellerContactInformation:
										'深圳市南山区阳光科创中心B座35楼3501室0755-86620031',
									sellerOpenBank:
										'上海浦东发展银行深圳福田支行79290154740001858',
									buyerTaxpayerName: '91110108098166163W',
									buyerContactInformation:
										'北京市海淀区中关村南大街S号1区689号楼803室85875300',
									buyerOpenBank: '农业银行北京西长安街支行11211101040000433',
									checkPass: true,
									manualCheckPass: true,
									createTime: '2018-10-25 18:02:28',
									submitter: 5,
									submitterName: '郭子俊',
									attachment: {
										id: 2369,
										attachType: 'invoices',
										attachPath:
											'/home/chainFileStore/20181025/commonInvoice/432/invoices',
										attachName: 'ss01.jpg',
										uploadTime: '2018-10-25 18:02:28',
										uploadUser: 5,
										businessType: 10,
										businessId: 432,
										attachmentKey: 'invoices',
										uploaderCompanyCode: 'COM00000001',
										userName: '郭子俊',
										url: '/20181025/commonInvoice/432/invoices/ss01.jpg'
									}
								},
								{
									id: 433,
									subjectId: 132,
									buyer: '中金甲子(北京)投资基金管理有限公司',
									code: '4403173130',
									number: '16067638',
									isCancel: false,
									amount: 100000.0,
									totalTaxAmount: 5660.38,
									totalPriceAmount: 94339.62,
									totalQuantity: 0,
									invoiceDate: '2018-06-13 00:00:00',
									checkCode: '',
									sellerTaxpayerName: '91440300MA5DDP4U88',
									sellerContactInformation:
										'深圳市南山区阳光科创中心B座35楼3501室0755-86620031',
									sellerOpenBank:
										'上海浦东发震银行深圳福田支行79290154740001858',
									buyerTaxpayerName: '91110108098166163W',
									buyerContactInformation:
										'北京市海淀区中关村南大街5号1区689号楼803室85875300',
									buyerOpenBank: '农业银行北京西长安街支行11211101040000433',
									checkPass: true,
									manualCheckPass: false,
									createTime: '2018-10-25 18:02:28',
									submitter: 5,
									submitterName: '郭子俊',
									attachment: {
										id: 2370,
										attachType: 'invoices',
										attachPath:
											'/home/chainFileStore/20181025/commonInvoice/433/invoices',
										attachName: 'ss02.jpg',
										uploadTime: '2018-10-25 18:02:28',
										uploadUser: 5,
										businessType: 10,
										businessId: 433,
										attachmentKey: 'invoices',
										uploaderCompanyCode: 'COM00000001',
										userName: '郭子俊',
										url: '/20181025/commonInvoice/433/invoices/ss02.jpg'
									}
								}
							],
							acceptCount: 1,
							acceptAmount: 100000.0
						},
						attachments: {
							businessType: 16,
							businessId: 132,
							contracts: [],
							invoices: [],
							orders: [
								{
									id: 2371,
									attachType: 'orders',
									attachPath:
										'/home/chainFileStore/20181025/AccountReceiveableFiles/132/orders',
									attachName: 'fail.png',
									uploadTime: '2018-10-25 18:02:28',
									uploadUser: 5,
									businessType: 16,
									businessId: 132,
									attachmentKey: 'orders',
									uploaderCompanyCode: 'COM00000001',
									userName: '郭子俊',
									url: '/20181025/AccountReceiveableFiles/132/orders/fail.png'
								}
							],
							deliveryNotes: [],
							receiptNotes: [],
							accountStatements: [],
							qualityInspections: [],
							others: [],
							dueDiligence: [],
							repayFiles: [],
							makeLoanFiles: [],
							factoringContracts: [],
							factoringVideos: [],
							zhdFiles: [],
							bankAccountFiles: [],
							confirmFiles: [],
							resolutionFiles: [],
							otherMaterialInspections: []
						}
					}
				]
			}
		}
	},
	{
		// 新增应收账款 post
		url: '/biz/ac/add',
		response: options => {
			/*
        {
            endTime: "2018-11-01",// 到期日期
            amount: 7899898,// 金额
            // 发票json
            invoiceJson: '[{"buyer":"中金甲子(北京)投资基金管理有限公司","code":"4403173130","number":"16067638","amount":"100000","invoiceDate":"2018年6月13日","checkCode":"","sellerOpenBank":"上海浦东发震银行深圳福田支行79290154740001858","sellerTaxpayerName":"91440300MA5DDP4U88","sellerContactInformation":"深圳市南山区阳光科创中心B座35楼3501室0755-86620031","buyerTaxpayerName":"91110108098166163W","buyerOpenBank":"农业银行北京西长安街支行11211101040000433","buyerContactInformation":"北京市海淀区中关村南大街5号1区689号楼803室85875300","totalPriceAmount":"94339.62","totalTaxAmount":"5660.38","checkPass":true,"taxAttach":"发票.jpeg"}]',

            // 发票文件
            invoices: (binary),
            businessId: 143
        }
    */
			return {
				code: '0',
				msg: '成功',
				data: {}
			}
		}
	},
	{
		// 合同签署详情 get
		url: '/contract/detail',
		response: options => {
			/*
        {
            endTime: "2018-11-01",// 到期日期
            amount: 7899898,// 金额
            // 发票json
            invoiceJson: '[{"buyer":"中金甲子(北京)投资基金管理有限公司","code":"4403173130","number":"16067638","amount":"100000","invoiceDate":"2018年6月13日","checkCode":"","sellerOpenBank":"上海浦东发震银行深圳福田支行79290154740001858","sellerTaxpayerName":"91440300MA5DDP4U88","sellerContactInformation":"深圳市南山区阳光科创中心B座35楼3501室0755-86620031","buyerTaxpayerName":"91110108098166163W","buyerOpenBank":"农业银行北京西长安街支行11211101040000433","buyerContactInformation":"北京市海淀区中关村南大街5号1区689号楼803室85875300","totalPriceAmount":"94339.62","totalTaxAmount":"5660.38","checkPass":true,"taxAttach":"发票.jpeg"}]',
            // 发票文件
            invoices: (binary),
            businessId: 143
        }
    */
			return {
				code: '0',
				msg: '成功',
				data: {
					code: '0',
					msg: '成功',
					data: {
						contractDetailVO: {
							businessId: 143,
							signType: 1, // 签署类型 1-线上  2线下
							type: 1, //  合同类型 1-保理合同 2-确权文件
							fileName: '动产申请表.pdf', //  线上签署返回
							remark: '343434',
							endDate: '2018-11-03 00:00:00',
							statusName: '已签署',
							contractCode: '897563a4-9146-4431-b21b-33f2d4d38fd8', // 给众签的合同编号 线上签署返回
							no: '434334', // 本系统合同编号
							//  线上签署返回
							companyList: [
								{
									grade: 0,
									companyName: '深圳前海闪电保理有限公司',
									status: '已完成'
								},
								{
									grade: 1,
									companyName: '深圳市潜动力科技有限公司',
									status: '已完成'
								}
							],
							// 线下签署返回
							attachmentList: [
								{
									id: 2368,
									attachType: 'contracts',
									attachPath:
										'/home/chainFileStore/20181025/common/136/contracts',
									attachName: '线上合同.pdf',
									companyCode: 'COM1536802996953',
									uploadTime: '2018-10-25 18:02:28',
									uploadUser: 5,
									businessType: 4,
									businessId: 136,
									attachmentKey: 'contracts',
									uploaderCompanyCode: 'COM00000001',
									userName: '郭子俊',
									url: '/20181025/common/136/contracts/线上合同.pdf'
								}
							]
						},
						confirmationVO: {
							businessId: 143,
							signType: 1,
							type: 2,
							fileName: '动产申请表.pdf', // 线上签署返回
							remark: '3223',
							endDate: '2018-11-03 00:00:00',
							statusName: '待签署',
							contractCode: '996b257a-0650-4454-981f-a82972139030', // 给众签的合同编号 线上签署返回
							no: '3233', // 本系统合同编号
							// 线上签署返回
							companyList: [
								{
									grade: 2,
									companyName: '深圳前海无限科技有限公司2',
									status: '待签署'
								},
								{
									grade: 0,
									companyName: '深圳前海闪电保理有限公司',
									status: '待签署'
								},
								{
									grade: 1,
									companyName: '深圳市潜动力科技有限公司',
									status: '待签署'
								}
							],
							// 线下签署返回
							attachmentList: [
								{
									id: 2368,
									attachType: 'contracts',
									attachPath:
										'/home/chainFileStore/20181025/common/136/contracts',
									attachName: '线上合同.pdf',
									companyCode: 'COM1536802996953',
									uploadTime: '2018-10-25 18:02:28',
									uploadUser: 5,
									businessType: 4,
									businessId: 136,
									attachmentKey: 'contracts',
									uploaderCompanyCode: 'COM00000001',
									userName: '郭子俊',
									url: '/20181025/common/136/contracts/线上合同.pdf'
								}
							]
						}
					}
				}
			}
		}
	},
	{
		// 公司列表 get
		url: '/company/pairs',
		response: options => {
			return {
				code: '0',
				msg: '成功',
				data: {
					'13': '深圳前海无限科技'
				}
			}
		}
	},
	{
		// 业务放款查询 get
		url: '/biz/select/make-loan',
		response: options => {
			/*
            {
                "businessId":137
            }
            */
			return {
				code: '0',
				msg: '成功',
				data: {
					actualMakeLoanAmount: 0, // 实际放款
					acceptAmount: 0, // 受理金额
					accountsReceivableAmount: 0, //
					financingInterestRate: 0, // 融资利率
					shouldMakeLoanAmount: 0, // 应放金额
					serviceAmount: 0, // 服务费
					list: [
						{
							makeLoanDate: '2018-10-23 00:00:00', // 放款日期
							latestRepayDate: '2018-10-28 00:00:00', // 还款日期
							amount: 1000,
							confirmorName: '郭子俊',
							confirmDate: '2018-11-05 22:45:05', // 确认日期
							overDue: false // 逾期
						},
						{
							makeLoanDate: '2018-10-24 00:00:00',
							latestRepayDate: '2018-10-29 00:00:00',
							amount: 10000,
							confirmorName: '郭子俊',
							confirmDate: '2018-11-05 22:45:05',
							overDue: false
						},
						{
							makeLoanDate: '2018-10-24 00:00:00',
							latestRepayDate: '2018-10-29 00:00:00',
							amount: 10000,
							confirmorName: '郭子俊',
							confirmDate: '2018-11-05 22:45:05',
							overDue: false
						},
						{
							makeLoanDate: '2018-10-23 00:00:00',
							latestRepayDate: '2018-11-02 00:00:00',
							amount: 1000,
							confirmorName: '郭子俊',
							confirmDate: '2018-11-05 22:45:05',
							overDue: false
						},
						{
							makeLoanDate: '2018-10-24 00:00:00',
							latestRepayDate: '2018-11-03 00:00:00',
							amount: 1000,
							confirmorName: '郭子俊',
							confirmDate: '2018-11-05 22:45:05',
							overDue: false
						},
						{
							makeLoanDate: '2018-11-01 00:00:00',
							latestRepayDate: '2018-11-06 00:00:00',
							amount: 2000,
							confirmorName: '郭二',
							confirmDate: '2018-11-05 22:45:05',
							overDue: false
						},
						{
							makeLoanDate: '2018-11-01 00:00:00',
							latestRepayDate: '2018-11-11 00:00:00',
							amount: 1000,
							confirmorName: '郭二',
							confirmDate: '2018-11-05 22:45:05',
							overDue: false
						},
						{
							makeLoanDate: '2018-11-01 00:00:00',
							latestRepayDate: '2018-11-11 00:00:00',
							amount: 1000,
							confirmorName: '郭二',
							confirmDate: '2018-11-05 22:45:05',
							overDue: false
						}
					],
					contractSigned: false
				}
			}
		}
	},
	{
		// 业务还款查询 get
		url: '/biz/select/repay',
		response: options => {
			/*
            {
                "businessId":137
            }
            */
			return {
				code: '0',
				msg: '成功',
				data: {
					shouldRepayAmount: 0, // 应还金额
					shouldRepayCapital: 0, // 应还本金
					shouldRepayInterest: 0, // 应还利息
					remainShouldRepayAmount: 0, // 剩余应还金额
					actualRepayCapital: 0, // 实还金额
					actualRepayInterest: 0, // 实还利息
					list: [
						{
							repayDate: '2018-10-27 00:00:00',
							amount: 2000,
							confirmorName: '郭二',
							confirmDate: '2018-11-05 22:45:23'
						},
						{
							repayDate: '2018-10-28 00:00:00',
							amount: 500,
							confirmorName: '郭子俊',
							confirmDate: '2018-11-05 22:45:23'
						},
						{
							repayDate: '2018-10-28 00:00:00',
							amount: 1000,
							confirmorName: '郭子俊',
							confirmDate: '2018-11-05 22:45:23'
						},
						{
							repayDate: '2018-10-29 00:00:00',
							amount: 500,
							confirmorName: '郭子俊',
							confirmDate: '2018-11-05 22:45:23'
						},
						{
							repayDate: '2018-10-29 00:00:00',
							amount: 5000,
							confirmorName: '郭子俊',
							confirmDate: '2018-11-05 22:45:23'
						},
						{
							repayDate: '2018-11-10 00:00:00',
							amount: 1000,
							confirmorName: '郭二',
							confirmDate: '2018-11-05 22:45:23'
						},
						{
							repayDate: '2018-11-15 00:00:00',
							amount: 2000,
							confirmorName: '郭二',
							confirmDate: '2018-11-05 22:45:23'
						}
					],
					payInterestType: 0,
					contractSigned: false
				}
			}
		}
	},
	{
		// 收款列表 get
		url: '/biz/make-loan/list',
		response: options => {
			/*
        	{
        		makeLoanStartDate: "20181010",//放款开始日期
        		makeLoanEndDate: "20181010",//放款结束日期
        		latestRepayStartDate:"20181010",//还款开始日期
        		latestRepayEndDate:"20181010",//还款结束日期
        		overDueStatus:"0",//逾期状态 0全部 1是 2否
        		makeLoanCode:"33"//资金编号
        	}

        	 */
			return {
				code: '0',
				msg: '成功',
				data: {
					total: 8,
					list: [
						{
							id: 111,
							businessId: 137,
							createTime: '2018-11-05 22:45:05',
							makeLoanDate: '2018-10-23 00:00:00',
							capital: 1000,
							confirmor: 5,
							latestRepayDate: '2018-10-28 00:00:00',
							overDueDays: 0,
							repayCapital: 1500,
							repayInterest: 0,
							remainRepayInterest: 0.277778,
							serviceAmount: 0,
							remainRepayCapital: 0,
							businessCode: 'BL20181105215056567',
							overDue: false
						},
						{
							id: 112,
							businessId: 137,
							createTime: '2018-11-05 22:45:05',
							makeLoanDate: '2018-10-23 00:00:00',
							capital: 1000,
							confirmor: 5,
							latestRepayDate: '2018-11-02 00:00:00',
							overDueDays: 0,
							repayCapital: 0,
							repayInterest: 0,
							remainRepayInterest: 0.555556,
							serviceAmount: 0,
							remainRepayCapital: 0,
							businessCode: 'BL20181105215056567',
							overDue: false
						},
						{
							id: 113,
							businessId: 137,
							createTime: '2018-11-05 22:45:05',
							makeLoanDate: '2018-11-01 00:00:00',
							capital: 1000,
							confirmor: 6,
							latestRepayDate: '2018-11-11 00:00:00',
							overDueDays: 0,
							repayCapital: 0,
							repayInterest: 0,
							remainRepayInterest: 2.777778,
							serviceAmount: 0,
							remainRepayCapital: 0,
							businessCode: 'BL20181105215056567',
							overDue: false
						},
						{
							id: 114,
							businessId: 137,
							createTime: '2018-11-05 22:45:05',
							makeLoanDate: '2018-11-01 00:00:00',
							capital: 2000,
							confirmor: 6,
							latestRepayDate: '2018-11-06 00:00:00',
							overDueDays: 0,
							repayCapital: 3000,
							repayInterest: 0,
							remainRepayInterest: 2.777778,
							serviceAmount: 0,
							remainRepayCapital: 0,
							businessCode: 'BL20181105215056567',
							overDue: false
						},
						{
							id: 115,
							businessId: 137,
							createTime: '2018-11-05 22:45:05',
							makeLoanDate: '2018-11-01 00:00:00',
							capital: 1000,
							confirmor: 6,
							latestRepayDate: '2018-11-11 00:00:00',
							overDueDays: 0,
							repayCapital: 0,
							repayInterest: 0,
							remainRepayInterest: 2.777778,
							serviceAmount: 0,
							remainRepayCapital: 0,
							businessCode: 'BL20181105215056567',
							overDue: false
						}
					],
					pageNum: 1,
					pageSize: 5,
					size: 5,
					startRow: 1,
					endRow: 5,
					pages: 2,
					prePage: 0,
					nextPage: 2,
					isFirstPage: true,
					isLastPage: false,
					hasPreviousPage: false,
					hasNextPage: true,
					navigatePages: 8,
					navigatepageNums: [1, 2],
					navigateFirstPage: 1,
					navigateLastPage: 2,
					firstPage: 1,
					lastPage: 2
				}
			}
		}
	},
	{
		// 还款列表 get
		url: '/biz/repay/list',
		response: options => {
			/*
        		{
        			applyStartDate:"20180101",//申请开始日期
        			applyEndDate:"20180101",//申请结束日期
        			repayStartDate:"20180101",//申请结束日期
        			repayEndDate:"20180101",//申请结束日期
        			confirmStatus:"0"//0全部 1未确认 2已确认 3拒绝
        		}
        	 */
			return {
				code: '0',
				msg: '成功',
				data: {
					total: 7,
					list: [
						{
							id: 77,
							businessId: 137,
							createTime: '2018-11-05 22:45:23',
							repayDate: '2018-10-28 00:00:00',
							amount: 500,
							confirmor: 5,
							capital: 0,
							interest: 0,
							updateTime: '2018-11-05 22:45:23',
							submitter: 0,
							confirmStatus: 0,
							companyName: '深圳前海无限科技有限公司',
							confirmStatusName: '未知',
							businessCode: 'BL20181105215056567'
						},
						{
							id: 78,
							businessId: 137,
							createTime: '2018-11-05 22:45:23',
							repayDate: '2018-10-28 00:00:00',
							amount: 1000,
							confirmor: 5,
							capital: 0,
							interest: 0,
							updateTime: '2018-11-05 22:45:23',
							submitter: 0,
							confirmStatus: 0,
							companyName: '深圳前海无限科技有限公司',
							confirmStatusName: '未知',
							businessCode: 'BL20181105215056567'
						},
						{
							id: 79,
							businessId: 137,
							createTime: '2018-11-05 22:45:23',
							repayDate: '2018-11-10 00:00:00',
							amount: 1000,
							confirmor: 6,
							capital: 0,
							interest: 0,
							updateTime: '2018-11-05 22:45:23',
							submitter: 0,
							confirmStatus: 0,
							companyName: '深圳前海无限科技有限公司',
							confirmStatusName: '未知',
							businessCode: 'BL20181105215056567'
						},
						{
							id: 80,
							businessId: 137,
							createTime: '2018-11-05 22:45:23',
							repayDate: '2018-11-15 00:00:00',
							amount: 2000,
							confirmor: 6,
							capital: 0,
							interest: 0,
							updateTime: '2018-11-05 22:45:23',
							submitter: 0,
							confirmStatus: 0,
							companyName: '深圳前海无限科技有限公司',
							confirmStatusName: '未知',
							businessCode: 'BL20181105215056567'
						},
						{
							id: 83,
							businessId: 137,
							createTime: '2018-11-05 22:45:23',
							repayDate: '2018-10-29 00:00:00',
							amount: 500,
							confirmor: 5,
							capital: 0,
							interest: 0,
							updateTime: '2018-11-05 22:45:23',
							submitter: 0,
							confirmStatus: 0,
							companyName: '深圳前海无限科技有限公司',
							confirmStatusName: '未知',
							businessCode: 'BL20181105215056567'
						}
					],
					pageNum: 1,
					pageSize: 5,
					size: 5,
					startRow: 1,
					endRow: 5,
					pages: 2,
					prePage: 0,
					nextPage: 2,
					isFirstPage: true,
					isLastPage: false,
					hasPreviousPage: false,
					hasNextPage: true,
					navigatePages: 8,
					navigatepageNums: [1, 2],
					navigateFirstPage: 1,
					navigateLastPage: 2,
					firstPage: 1,
					lastPage: 2
				}
			}
		}
	},
	{
		// 线下合同上传 post
		url: '/contract/offline/uploadContract',
		response: options => {
			/*
				{
					type: "1", //1-保理合同 2-确认文件 是
					remark:"32",// 备注 否
					no:"323232"//编号 是
				}
			 */
			return {
				code: '0',
				msg: '成功',
				data: {}
			}
		}
	},
	{
		// 线上合同上传 post
		url: '/contract/online/uploadAndCreadContract',
		response: options => {
			/*
            {
                type: "1", //1-保理合同 2-确认文件 是
                applicationNum:"32",// 业务编号 是
                endDate:"2018-01-01",// 合同签署有效期 是
                // 签署公司jsonArray 是
                companyList: [{
                    name: "",
                    id: "",
                    type: "" //合同签署方 0甲方、1乙方、2丙方
                }],
                contracts:"",// 合同文件 是
                remark:"32",// 备注 否
                no:"323232"//编号 是
            }
         */
			return {
				code: '0',
				msg: '成功',
				data: {}
			}
		}
	},
	{
		// 合同详情 get
		url: '/contract/detail',
		response: options => {
			/*
				{
					businessId:"123"
				}
		 	*/
			return {
				code: '0',
				msg: '成功',
				data: {
					// 线下返回结果示例
					contractDetailVO: {
						businessId: 33,
						signType: 2,
						type: 1,
						remark: '备注',
						statusName: '已签署',
						attachmentList: [
							{
								id: 1609,
								attachType: 'signedContracts',
								attachPath:
									'D:\\\\work\\20180927\\hospital\\33\\signedContracts',
								attachName: '动产申请表.pdf',
								uploadTime: '2018-09-27 20:25:29',
								uploadUser: 5,
								businessType: 4,
								businessId: 33,
								attachmentKey: 'signedContracts',
								uploaderCompanyCode: 'COM00000001',
								userName: '郭子俊',
								url: '\\20180927\\hospital\\33\\signedContracts\\动产申请表.pdf'
							}
						]
					},
					// 线上签署返回示例
					confirmationVO: {
						businessId: 35,
						signType: 1,
						type: 1,
						fileName: '动产申请表.pdf',
						remark: '备注',
						endDate: '2018-10-01 00:00:00',
						statusName: '待签署',
						contractCode: '53d83961-68f2-4ba0-b2cf-56685d42e7fa',
						companyList: [
							{
								grade: 0,
								companyName: '深圳前海闪电保理有限公司',
								status: '待签署'
							},
							{
								grade: 1,
								companyName: '深圳市潜动力科技有限公司',
								status: '待签署'
							}
						]
					}
				}
			}
		}
	},
	{
		// 还款新增 post
		url: '/biz/add/repay',
		response: options => {
			/*
				{
						amount:"123",
						businessId:"3332",
						repayDate:"20181107"//还款日期
				}
			*/
			return {
				code: '0',
				msg: '成功',
				data: {}
			}
		}
	},
	{
		// 共托管账户新增 get
		url: '/biz/joint-or-escrow-account/get',
		response: options => {
			/*
				{
						id:"" //businessId
				}
			*/
			return {
				code: '0',
				msg: '成功',
				data: {
					bankAccountName: '账号名称',
					bankAccountNumber: '账号',
					bankName: '开户行',
					bankAccountType: '',
					bankAccountTypeName: '账号类型名称',
					exists: 'false' //数据是否存在
				}
			}
		}
	}
]
