import Mock from 'mockjs'

const mockRandom = Mock.Random

mockRandom.extend({})

Mock.setup({
	Timeout: '200-600'
})

const dataTemplet = response => {
	console.log('enter mock')
	return Mock.mock({
		code: (response && response.code) || 0,
		data: (response && response.data) || response || {},
		message: response.message || 'success'
	})
}

const createData = conf => {
	if (
		typeof conf.response === 'function' ||
		typeof conf.response === 'object'
	) {
		Mock.mock(RegExp(conf.url + '.*'), options => {
			return dataTemplet(
				typeof conf.response === 'function'
					? conf.response(options)
					: conf.response
			)
		})
	} else {
		process.env.NODE_ENV !== 'production' && console.warn('invalid response')
		return false
	}
}

/*  main.js 中将方法createData挂载为vue属性 $mock

    单文件组件创建mock数据必须在发起数据请求之前初始化mock

    respons 参数值为返回对象 code为接口状态码 data 数据 message 提示

    示例一（接口无参数传递）：
        this.$mock({
            url : '需要mock的url'
            response: {
               code ： 0，
               data ： {
                    nickname: '@cname', // 返回字段名为nickname 值为随机中文名的数据
               }，
               message ： "返回成功"
	        }
        })

    示例二（接口有参数传递）：
        this.$mock({
            url : '需要mock的url'
            response（options) {
                // options 中可获取接口参数  根据接口参数结合mock语法生成数据返回即可
               return {
                    code ： 0，
                    data ： {
                        nickname: '@cname', // 返回字段名为nickname 值为随机中文名的数据
                    }，
                    message ： "返回成功"
               }
	        }
        })

    mock语法案例 http://mockjs.com/examples.html
 */
export { createData }
