import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			redirect: '/login',
			component: () => import(/* webpackChunkName: "main" */ '@/views/main'),
			children: [
				// {
				// 	path: 'index',
				// 	name: 'index',
				// 	component: () =>
				// 		import(/* webpackChunkName: "guide" */ '@login/guide')
				// },
				{
					path: 'login',
					name: 'login',
					component: () =>
						import(/* webpackChunkName: "login" */ '@login/login')
				},
				{
					path: 'register',
					name: 'register',
					component: () =>
						import(/* webpackChunkName: "register" */ '@login/register')
				},
				{
					path: 'face',
					name: 'face',
					component: () =>
						import(/* webpackChunkName: "face" */ '@/views/faceModule/faceVerify')
				},
				{
					path: 'controller',
					name: 'controller',
					component: () =>
						import(/* webpackChunkName: "controller" */ '@/views/controller'),
					children: [
						{
							path: 'home',
							name: 'home',
							component: () =>
								import(/* webpackChunkName: "home" */ '@home/home')
						},
						{
							path: 'bussinessBase',
							name: 'bussinessBase',
							component: () =>
								import(/* webpackChunkName: "bussinessBase" */ '@bussiness/bussinessBase')
						},
						{
							path: 'bussinessRiskData',
							name: 'bussinessRiskData',
							component: () =>
								import(/* webpackChunkName: "bussinessRiskData" */ '@bussiness/bussinessRiskData')
						},
						{
							path: 'taskAndNotice',
							name: 'taskAndNotice',
							component: () =>
								import(/* webpackChunkName: "taskAndNotice" */ '@notice/taskAndNotice')
						},
						{
							path: 'salesList',
							name: 'salesList',
							component: () =>
								import(/* webpackChunkName: "salesList" */ '@sales/salesList')
						},
						{
							path: 'fundDetail',
							name: 'fundDetail',
							component: () =>
								import(/* webpackChunkName: "fundDetail" */ '@sales/fundDetail')
						},
						{
							path: 'saleOpration',
							name: 'saleOpration',
							component: () =>
								import(/* webpackChunkName: "saleOpration" */ '@sales/saleOpration')
						},
						{
							path: 'repayDetail',
							name: 'repayDetail',
							component: () =>
								import(/* webpackChunkName: "repayDetail" */ '@sales/repayDetail')
						}
					]
				}
			]
		}
	]
})
