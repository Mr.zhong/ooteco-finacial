import Vue from 'vue'
import Vuex from 'vuex'

import login from './loginModule/login'
import { menu_list } from './menu_list'
import { sub_title } from './sub_title'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		WINDOW_H: 0,
		WINDOW_W: 0,
		menu_list,
		sub_title: Object.assign({}, utils.deepClone(sub_title)),
		userInfo: null
	},
	mutations: {},
	actions: {},
	modules: {
		login
	}
})
