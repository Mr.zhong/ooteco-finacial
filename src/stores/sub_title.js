const sub_title = {
	bussinessBase: {
		title: '基本资料',
		buttons: [
			{
				text: '资料包下载',
				event: 'dataPackageDowmload'
			}
		]
	},
	bussinessRiskData: {
		title: '风控资料',
		buttons: []
	},
	taskAndNotice: {
		title: '任务通知',
		buttons: [
			{
				text: '标记已读',
				event: 'markNotice'
			},
			{
				text: '删除',
				event: 'deleteNotice'
			}
		]
	},
	salesList: {
		title: '业务列表',
		buttons: [
			{
				text: '查看',
				event: 'infoDetail'
			}
		]
	},
	repayDetail: {
		title: '还款明细',
		buttons: []
	},
	fundDetail: {
		title: '收款明细',
		buttons: []
	},
	saleOpration: {
		title: '业务详情',
		buttons: [
			{
				text: '业务包下载',
				event: 'servicePackageDowmload'
			}
		]
	}
}
export { sub_title }
