export default {
	dialogs: [
		{
			centerDialogVisible: false,
			files: [
				{
					url: 'tripartite/requestOcr',
					flag: 'idcard_agent_face',
					drag: true,
					accept: 'image/*',
					data: { apiType: 1, type: 1, side: 'face' },
					form_key: 'operator'
				},
				{
					url: 'tripartite/requestOcr',
					flag: 'idcard_agent_back',
					accept: 'image/*',
					drag: true,
					data: { apiType: 1, type: 1, side: 'back' },
					form_key: 'operator'
				},
				{
					dont_auto_upload: true,
					drag: true,
					flag: 'power',
					accept: 'image/*',
					form_key: 'authorizationFile'
				}
			],
			form_info: [
				{
					lab: '身份证号',
					text: '',
					key: 'num',
					form_key: 'operatorCardNumber'
				},
				{
					lab: '出生日期',
					text: '',
					key: 'birth',
					form_key: 'operatorBirthTime',
					filter: val => {
						return (
							val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
						)
					}
				},
				{
					lab: '姓名',
					text: '',
					key: 'name',
					form_key: 'operatorName'
				},
				{
					lab: '到期日期',
					text: '',
					key: 'end_date',
					form_key: 'operatorCardEndTime',
					filter: val => {
						return (
							val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
						)
					}
				},
				{
					lab: '性别',
					text: '',
					key: 'sex',
					form_key: 'operatorGender'
				},
				{
					lab: '民族',
					text: '',
					key: 'nationality',
					form_key: 'operatorNation'
				},
				{
					lab: '核查结果',
					text: '',
					key: 'success',
					form_key: 'operatorCarcCheckResult',
					filter: val => {
						return val ? '通过' : '未通过'
					}
				},
				{
					lab: '',
					text: '',
					key: 'start_date',
					form_key: 'operatorCardStartTime',
					filter: val => {
						return (
							val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
						)
					}
				}
			]
		},
		{
			centerDialogVisible: false
		},
		{
			centerDialogVisible: false
		}
	],
	tables: [
		{
			title: '',
			form_key: 'companyInfo',
			items: [
				{
					name: '公司名称',
					text: '',
					form_key: 'companyName'
				},
				{
					name: '成立日期',
					text: '',
					form_key: 'companyCreateTime',
					filter: val => {
						return val.substr(0, 10)
					}
				},
				{
					name: '公司状态',
					text: '',
					form_key: 'companyStatus',
					filter: val => {
						return val === 1 ? '激活' : '锁定'
					}
				},
				{
					name: '统一社会信用码',
					text: '',
					form_key: 'companyCreditCode'
				},
				{
					name: '营业执照到期日',
					text: '',
					form_key: 'companyEndTime',
					filter: val => {
						return val.substr(0, 10)
					}
				},
				{
					name: '企业认证',
					text: '',
					form_key: 'approvalStatusName'
				},
				{
					name: '主体类型',
					text: '',
					form_key: 'companyType'
				},
				{
					name: '企业注册地址',
					text: '',
					form_key: 'companyAddress',
					total: true
				},
				{
					name: '相关附件',
					type: 'file',
					form_key: 'TODO'
				}
			]
		},
		{
			title: '法人资料',
			form_key: 'legaluser',
			items: [
				{
					name: '类型',
					text: '法人'
				},
				{
					name: '姓名',
					text: '',
					form_key: 'userName'
				},
				{
					name: '性别',
					text: '',
					form_key: 'userGender'
				},
				{
					name: '证件开始日期',
					text: '',
					form_key: 'idCardStartTime',
					filter: val => {
						return val.substr(0, 10)
					}
				},
				{
					name: '证件号码',
					text: '',
					form_key: 'idCardNumber'
				},
				{
					name: '证件到期日',
					text: '',
					form_key: 'idCardEndTime',
					filter: val => {
						return val.substr(0, 10)
					}
				},
				{
					name: '民族',
					text: '',
					form_key: 'nation'
				},
				{
					name: '核查结果',
					text: '',
					form_key: 'checkResultName'
				},
				{
					name: '相关附件',
					inline: true,
					type: 'file',
					form_key: 'TODO'
				}
			]
		},
		{
			title: '经办资料',
			form_key: 'operator',
			items: [
				{
					name: '类型',
					text: '经办人'
				},
				{
					name: '姓名',
					text: '',
					form_key: 'userName'
				},
				{
					name: '性别',
					text: '',
					form_key: 'userGender'
				},
				{
					name: '证件开始日期',
					text: '',
					form_key: 'idCardStartTime',
					filter: val => {
						return val.substr(0, 10)
					}
				},
				{
					name: '证件号码',
					text: '',
					form_key: 'idCardNumber'
				},
				{
					name: '证件到期日',
					text: '',
					form_key: 'idCardEndTime',
					filter: val => {
						return val.substr(0, 10)
					}
				},
				{
					name: '民族',
					text: '',
					form_key: 'nation'
				},
				{
					name: '核查结果',
					text: '',
					total: true,
					form_key: 'checkResultName'
				},
				{
					name: '联系手机',
					text: '',
					form_key: 'cellphone'
				},
				{
					name: '联系邮箱',
					text: '',
					total: true,
					form_key: 'email'
				},
				{
					name: '法人委托书',
					text: '',
					type: 'file'
					// form_key: 'checkResultName'
				},
				{
					name: '相关附件',
					text: '',
					type: 'file',
					inline: true
					// form_key: 'checkResultName'
				}
			]
		},
		{
			title: '银行账户',
			form_key: 'companyBank',
			items: [
				{
					name: '开户行及网点',
					text: '',
					form_key: 'bankName'
				},
				{
					name: '账户名',
					text: '',
					form_key: 'bankAccountName'
				},
				{
					name: '银行账号',
					text: '',
					form_key: 'bankAccountNumber'
				},
				{
					name: '开户省份',
					text: '',
					form_key: 'province'
				},
				{
					name: '开户城市',
					text: '',
					form_key: 'city'
				}
			]
		}
	],
	tableConf: {
		params: {},
		headers: [
			{
				label: '序号',
				prop: 'code',
				width: 50,
				style: {
					textAlign: 'center'
				}
			},
			{
				label: '附件名称',
				prop: 'name',
				type: 'important'
			},
			{
				label: '要求说明',
				prop: 'comment'
			},
			{
				label: '附件',
				prop: 'fileName',
				type: 'download',
				style: {
					textAlign: 'center'
				}
			}
		]
	}
}
