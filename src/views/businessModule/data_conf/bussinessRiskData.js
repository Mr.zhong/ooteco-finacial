export default {
	tableConf: {
		params: {},
		headers: [
			{
				label: '序号',
				prop: 'code',
				width: 50,
				style: {
					textAlign: 'center'
				}
			},
			{
				label: '附件名称',
				prop: 'name',
				type: 'important'
			},
			{
				label: '要求说明',
				prop: 'comment'
			},
			{
				label: '附件',
				prop: 'fileName',
				type: 'download',
				style: {
					textAlign: 'center'
				}
			},
			{
				label: '操作',
				prop: '',
				type: 'upload',
				width: 80,
				style: {
					textAlign: 'center'
				},
				sub_conf: {
					url: 'company/addRiskControlFile',
					type: 'text'
				}
			}
		]
	}
}
