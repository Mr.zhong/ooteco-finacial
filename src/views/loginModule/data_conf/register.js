export default {
	steps: ['营业执照', '法人身份', '经办人', '对公账户'],
	steps_title: ['上传营业执照', '上传法人身份证', '上传经办人身份证'],
	files: [
		[
			{
				url: 'tripartite/ocrBusinessLicense',
				class: 1,
				drag: true,
				flag: 'license',
				accept: 'image/*',
				form_key: 'businessLicence'
			}
		],
		[
			{
				url: 'tripartite/requestOcr',
				flag: 'idcard_person_face',
				drag: true,
				accept: 'image/*',
				data: { apiType: 1, type: 1, side: 'face' },
				form_key: 'legalUser'
			},
			{
				url: 'tripartite/requestOcr',
				flag: 'idcard_person_back',
				drag: true,
				accept: 'image/*',
				data: { apiType: 1, type: 1, side: 'back' },
				form_key: 'legalUser'
			}
		],
		[
			{
				url: 'tripartite/requestOcr',
				flag: 'idcard_agent_face',
				drag: true,
				accept: 'image/*',
				data: { apiType: 1, type: 1, side: 'face' },
				form_key: 'operator'
			},
			{
				url: 'tripartite/requestOcr',
				flag: 'idcard_agent_back',
				drag: true,
				accept: 'image/*',
				data: { apiType: 1, type: 1, side: 'back' },
				form_key: 'operator'
			},
			{
				dont_auto_upload: true,
				drag: true,
				flag: 'power',
				accept: 'image/*',
				form_key: 'authorizationFile'
			}
		]
	],
	form_info: [
		[
			{
				lab: '企业名称',
				text: '',
				key: 'name',
				form_key: 'companyName'
			},
			{
				lab: '成立日期',
				text: '',
				key: 'establish_date',
				filter: val => {
					return (
						val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
					)
				},
				form_key: 'companyCreateTime'
			},
			{
				lab: '统一社会信用代码',
				text: '',
				key: 'reg_num',
				form_key: 'companyCreditCode'
			},
			{
				lab: '主体类型',
				text: '',
				key: 'type',
				form_key: 'companyType'
			},
			{
				lab: '法定代表人',
				text: '',
				key: 'person',
				form_key: 'LegalName'
			},
			{
				lab: '到期日期',
				text: '',
				key: 'valid_period',
				form_key: 'companyEndTime',
				filter: val => {
					return (
						val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
					)
				}
			},
			{
				lab: '住所',
				text: '',
				key: 'address',
				form_key: 'companyAddress'
			},
			{
				lab: '',
				text: ''
			},
			{
				lab: '核查结果',
				text: '',
				key: 'verify',
				form_key: 'companyCheckResult',
				filter: val => {
					return val ? '通过' : '未通过'
				}
			},
			{
				lab: '',
				text: '',
				key: 'establish_date',
				form_key: 'companyCreateTime',
				filter: val => {
					return (
						val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
					)
				}
			}
		],
		[
			{
				lab: '身份证号',
				text: '',
				key: 'num',
				form_key: 'legalCardNumber'
			},
			{
				lab: '出生日期',
				text: '',
				key: 'birth',
				form_key: 'legalBirthTime',
				filter: val => {
					return (
						val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
					)
				}
			},
			{
				lab: '姓名',
				text: '',
				key: 'name',
				form_key: 'legalUserName'
			},
			{
				lab: '到期日期',
				text: '',
				key: 'end_date',
				form_key: 'legalEndTime',
				filter: val => {
					return (
						val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
					)
				}
			},
			{
				lab: '性别',
				text: '',
				key: 'sex',
				form_key: 'legalGender'
			},
			{
				lab: '民族',
				text: '',
				key: 'nationality',
				form_key: 'leagalNation'
			},
			{
				lab: '核查结果',
				text: '',
				key: 'success',
				form_key: 'legalCardCheckResult',
				filter: val => {
					return val ? '通过' : '未通过'
				}
			},
			{
				lab: '',
				text: '',
				key: 'start_date',
				form_key: 'legalStartTime',
				filter: val => {
					return (
						val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
					)
				}
			}
		],
		[
			{
				lab: '身份证号',
				text: '',
				key: 'num',
				form_key: 'operatorCardNumber'
			},
			{
				lab: '出生日期',
				text: '',
				key: 'birth',
				form_key: 'operatorBirthTime',
				filter: val => {
					return (
						val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
					)
				}
			},
			{
				lab: '姓名',
				text: '',
				key: 'name',
				form_key: 'operatorName'
			},
			{
				lab: '到期日期',
				text: '',
				key: 'end_date',
				form_key: 'operatorCardEndTime',
				filter: val => {
					return (
						val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
					)
				}
			},
			{
				lab: '性别',
				text: '',
				key: 'sex',
				form_key: 'operatorGender'
			},
			{
				lab: '民族',
				text: '',
				key: 'nationality',
				form_key: 'operatorNation'
			},
			{
				lab: '核查结果',
				text: '',
				key: 'success',
				form_key: 'operatorCarcCheckResult',
				filter: val => {
					return val ? '通过' : '未通过'
				}
			},
			{
				lab: '',
				text: '',
				key: 'start_date',
				form_key: 'operatorCardStartTime',
				filter: val => {
					return (
						val.substr(0, 4) + '-' + val.substr(4, 2) + '-' + val.substr(6, 2)
					)
				}
			}
		]
	]
}
