export default {
	checkBoxs: [
		{
			text: '补充资料',
			select: false,
			val: '5'
		},
		{
			text: '审批结果',
			select: false,
			val: '7'
		},
		{
			text: '合同签署',
			select: false,
			val: '4'
		},
		{
			text: '确权通知',
			select: false,
			val: '8'
		},
		{
			text: '放款通知',
			select: false,
			val: '9'
		},
		{
			text: '还款提醒',
			select: false,
			val: '6'
		},
		{
			text: '额度变更',
			select: false,
			val: '10'
		}
	],
	tableConf: {
		params: {
			pageNum: 1,
			pageSize: 10
		},
		pagination: {
			type: 1
		},
		otherColumn: {
			selection: {}
		},
		headers: [
			{
				label: '消息类型',
				prop: 'title',
				width: 180,
				type: 'badge'
			},
			{
				label: '消息标题',
				prop: 'detail'
			},
			{
				label: '生成时间',
				prop: 'createTime',
				style: {
					textAlign: 'center'
				}
			}
		]
	}
}
