export default {
	options: [
		{
			value: 'orders',
			label: '订单'
		},
		{
			value: 'deliveryNotes',
			label: '送货单'
		},
		{
			value: 'receiptNotes',
			label: '签收单'
		},
		{
			value: 'accountStatements',
			label: '对账单'
		},
		{
			value: 'qualityInspections',
			label: '质检证明'
		},
		{
			value: 'others',
			label: '其它'
		}
	],
	datasTableConf: {
		params: {},
		otherColumn: {
			index: {}
		},
		headers: [
			{
				label: '上传人',
				prop: 'userName',
				filter: val => {
					return (
						val ||
						JSON.parse(sessionStorage.getItem('userInfo')).operatorUserName
					)
				}
			},
			{
				label: '上传时间',
				prop: 'uploadTime',
				filter: val => {
					let date = new Date()
					let time =
						date.getFullYear() +
						'-' +
						(date.getMonth() + 1) +
						'-' +
						date.getDate() +
						' ' +
						date.getHours() +
						':' +
						date.getMinutes() +
						':' +
						date.getSeconds()
					return val || time
				}
			},
			{
				label: '附件',
				prop: 'attachName',
				type: 'download',
				style: {
					textAlign: 'center'
				}
			},
			{
				label: '操作',
				type: 'button',
				show: true,
				sub_conf: [
					{
						text: '删除',
						show: rowData => {
							if (rowData.recorded === true) {
								return false
							}
							return true
						},
						event: data => {
							data.store.table.data.splice(data.$index, 1)
						}
					}
				]
			}
		]
	},
	invoiceTableConf: {
		params: {},
		otherColumn: {
			index: {}
		},
		headers: [
			{
				label: '购方名称',
				prop: 'buyerName'
			},
			{
				label: '发票代码',
				prop: 'code'
			},
			{
				label: '发票号码',
				prop: 'number'
			},
			{
				label: '价税合计',
				prop: 'amount'
			},
			{
				label: '开票日期',
				prop: 'invoiceDate'
			},
			{
				label: '上传人',
				prop: 'submitterName',
				filter: val => {
					return val || '--'
				}
			},
			{
				label: '上传时间',
				prop: 'createTime',
				filter: val => {
					return val || '--'
				}
			},
			{
				label: '发票附件',
				prop: 'attachment',
				type: 'download',
				style: {
					textAlign: 'center'
				}
			},
			{
				label: '核验结果',
				prop: 'checkPass',
				filter: val => {
					return val === null ? '--' : val ? '通过' : '未通过'
				}
			},
			{
				label: '审核结果',
				prop: 'manualCheckPass',
				filter: val => {
					return typeof val === 'undefined' ? '--' : val ? '通过' : '未通过'
				}
			}
		]
	}
}
