export default {
	tableConf: {
		params: {},
		pagination: {
			type: 1
		},
		headers: [
			{
				label: '资金编号',
				prop: 'makeLoanCode'
			},
			{
				label: '业务编号',
				prop: 'businessCode'
			},
			{
				label: '放款时间',
				prop: 'makeLoanDate'
			},
			{
				label: '放款金额',
				prop: 'capital'
			},
			{
				label: '到期日期',
				prop: 'latestRepayDate'
			},
			{
				label: '是否逾期',
				prop: 'overDue',
				filter: val => {
					return val ? '是' : '否'
				}
			}
		]
	}
}
