export default {
	tableConf: {
		params: {},
		pagination: {
			type: 1
		},
		headers: [
			{
				label: '登记时间',
				prop: 'createTime'
			},
			{
				label: '资金编号',
				prop: 'repayCode'
			},
			{
				label: '业务编号',
				prop: 'businessCode'
			},
			{
				label: '还款日期',
				prop: 'repayDate'
			},
			{
				label: '还款金额',
				prop: 'amount'
			},
			{
				label: '确认状态',
				prop: 'confirmStatusName'
			},
			{
				label: '电子回单',
				prop: 'attachment',
				type: 'download',
				style: {
					textAlign: 'center'
				}
			}
		]
	}
}
