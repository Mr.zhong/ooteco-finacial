export default {
	tableConf: {
		radio: '',
		params: {
			makeLoanStatus: 0,
			repayStatus: 0
		},
		pagination: {
			type: 1
		},
		headers: [
			{
				label: '',
				prop: 'radio',
				type: 'select',
				width: 30
			},
			{
				label: '申请时间',
				prop: 'applyTime',
				width: 200
			},
			{
				label: '业务编号',
				prop: 'applyCode'
			},
			{
				label: '卖方(供应商)',
				prop: 'sellerName' //TODO  新添加字段名
			},
			{
				label: '买方(购货商)',
				prop: 'buyerName'
			},
			{
				label: '业务类型',
				prop: 'businessTypeName'
			},
			{
				label: '受理金额',
				prop: 'acceptAmount'
			},
			{
				label: '审批状态',
				prop: 'approvalStatusName'
			},
			{
				label: '合同状态',
				prop: 'contractStatusName'
			},
			{
				label: '放款状态',
				prop: 'makeLoanStatusName'
			},
			{
				label: '还款状态',
				prop: 'repayStatusName'
			}
		]
	},

	loanStatuslist: [
		{
			value: 0,
			label: '全部'
		},
		{
			value: 2,
			label: '待放款'
		},
		{
			value: 3,
			label: '已放款'
		}
	],

	repayStatusList: [
		{
			value: 0,
			label: '全部'
		},
		{
			value: 2,
			label: '待还款'
		},
		{
			value: 3,
			label: '已还款'
		}
	]
}
