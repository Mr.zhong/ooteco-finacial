const isProduction = process.env.NODE_ENV === 'production'
const path = require('path')
const webpack = require('webpack')
// const proectConfig = require('./project.config.json')
const manifest = require('./vendor-manifest.json')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
	.BundleAnalyzerPlugin

const resolve = dir => {
	return path.join(__dirname, dir)
}

let plugins = {
	base: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		}),
		new webpack.DllReferencePlugin({
			manifest
		})
	],
	production: [new BundleAnalyzerPlugin()],
	development: []
}

module.exports = {
	baseUrl: '/',
	lintOnSave: true,
	productionSourceMap: !isProduction,
	configureWebpack: conf => {
		conf.entry = {
			app: ['babel-polyfill', './src/main.js']
		}
		conf.plugins = [
			...conf.plugins,
			...plugins['base'],
			...plugins[process.env.NODE_ENV]
		]
	},
	chainWebpack: conf => {
		conf.resolve.alias
			.set('@img', resolve('src/assets/img'))
			.set('@js', resolve('src/assets/js'))
			.set('@css', resolve('src/assets/css'))
			.set('@com', resolve('src/components'))
			.set('@login', resolve('src/views/loginModule'))
			.set('@home', resolve('src/views/homeModule'))
			.set('@bussiness', resolve('src/views/businessModule'))
			.set('@notice', resolve('src/views/noticeModule'))
			.set('@sales', resolve('src/views/salesModule'))
	},
	devServer: {
		port: 9500,
		open: true,
		host: 'localhost',
		https: false,
		proxy: {
			'/api/': {
				target: 'http://localhost:8089/chain/fund-side/',
				// target: 'http://6053ac7.nat123.net:21957/chain/fund-side/',
				changeOrigin: true,
				pathRewrite: {
					'^/api/': ''
				}
			}
		}
	}
}
