const webpack = require('webpack')
const path = require('path')

module.exports = {
	entry: {
		vendor: ['vue', 'vue-router', 'vuex', 'element-ui', 'axios']
	},
	output: {
		path: path.join(__dirname, './public/js'),
		filename: 'dll.[name]_[hash:6].js',
		library: '[name]_[hash:6]'
	},
	plugins: [
		new webpack.DllPlugin({
			path: path.join(__dirname, './', '[name]-manifest.json'),
			name: '[name]_[hash:6]'
		})
	]
}
